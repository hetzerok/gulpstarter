var gulp = require('gulp'),
    pug = require('gulp-pug'),
    browserSync = require('browser-sync'),
    plumber = require('gulp-plumber'),
    less = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    spritesmith = require('gulp.spritesmith'),
    svgSprite = require('gulp-svg-sprite'),
    svgmin = require('gulp-svgmin'),
    cheerio = require('gulp-cheerio'),
    replace = require('gulp-replace');

var reload = browserSync.reload;

/* Настройка деррикторий */
var config = {
    src: {
        html: 'src/*.html',
        pug: 'src/pug/',
        less: 'src/less/',
        js: 'src/js/',
        images: 'src/img/',
        sprites: 'src/sprites/*.png',
        svgicons: 'src/svgicons/',
        svgsprite: 'src/svg/sprite.svg',
        svg: 'src/svg/*.svg',
        jsCustom: 'src/js/custom.js',
        jsVendor: 'src/js/vendor.js',
        style: 'src/css/style.less',
        formats: ['src/img/*.png', 'src/img/*.jpg', 'src/img/*.svg'],
        fonts: 'src/fonts/*.*',
    },
    dev: {
        html: 'dev/',
        css: 'dev/css/',
        js: 'dev/js/',
        maps: '/maps/',
        images: 'dev/img/',
        svgsprite: 'dev/svg/sprite.svg',
        svg: 'dev/svg/',
        style: 'dev/css/**/*.less',
        formats: 'dev/img/*.*',
        fonts: 'dev/fonts/*.*'
    },
    build: {
        html: 'build/',
        js: 'build/js/',
        jsMainFile: 'script.js',
        css: 'build/css/',
        img: 'build/img/',
        fonts: 'build/fonts/'
    },
    clean: ['build/**/*'],
    css: {
        versions: ['last 5 versions']
    },
    js: {
        final: 'all'
    },
    sprites: {
        imgName: 'sprites.png',
        cssName: 'sprites.less',
        relPath: 'img/',
        type: 'less',
        tpl: 'src/data/less.template.handlebars',
        padding: 5
    },
    svg: {
        tpl: 'src/data/svg.less.template.handlebars'
    }
};

gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: config.dev.html,
            index: "index.html"
        },
        browser: ["chrome"]
    });
});

gulp.task('pug', function () {
    return gulp.src(config.src.pug + '*.pug')
        .pipe(plumber())
        .pipe(pug())
        .pipe(gulp.dest(config.dev.html))
        .pipe(browserSync.stream());
});

gulp.task('less', function () {
    return gulp.src(config.src.less + '*.less')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(autoprefixer({
            browsers: config.css.versions
        }))
        .pipe(sourcemaps.write(config.dev.maps))
        .pipe(gulp.dest(config.dev.css))
});

gulp.task('concat', function () {
    return gulp.src([config.src.js + 'vendor/*.js', config.src.js + 'custom/*.js', config.src.js + '*.js'])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat(config.js.final + '.js'))
        .pipe(sourcemaps.write(config.dev.maps))
        .pipe(gulp.dest(config.dev.js));
});

gulp.task('img', function () {
    return gulp.src(config.src.images + '**')
        .pipe(plumber())
        .pipe(gulp.dest(config.dev.images));
});

gulp.task('sprites', function () {
    var sprite = gulp.src(config.src.sprites)
        .pipe(spritesmith({
            imgName: config.sprites.imgName,
            cssName: config.sprites.cssName,
            imgPath: config.sprites.relPath + config.sprites.imgName,
            cssFormat: config.sprites.type,
            padding: config.sprites.padding,
            cssTemplate: config.sprites.tpl,
            engineOpts: {
                "imagemagick": true
            }
        }));
    sprite.img.pipe(gulp.dest(config.src.images));
    sprite.img.pipe(gulp.dest(config.dev.images));
    sprite.css.pipe(gulp.dest(config.src.less + 'static/'));
});

gulp.task('svgsprites', function () {
    return gulp.src(config.src.svgicons + '*.svg')
        .pipe(plumber())
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[stroke]').removeAttr('stroke');
                $('[style]').removeAttr('style');
                $('defs').remove();
                $('style').remove();
            },
            parseOptions: {xmlMode: true}
        }))
        .pipe(replace('&gt;', '>'))
        .pipe(svgSprite({
            mode: {
                symbol: {
                    sprite: 'sprites.svg',
                    render: {
                        less: {
                            dest: 'svgsprites.less',
                            template: config.svg.tpl,
                        }
                    }
                }
            }
        }))
        .pipe(gulp.dest(config.src.svgicons));
});

gulp.task('ssmove', function () {
    return gulp.src(config.src.svgicons + 'symbol/sprites.svg')
        .pipe(gulp.dest(config.src.svg + 'sprites.svg'));
});

gulp.task('slmove', function () {
    return gulp.src(config.src.svgicons + 'symbol/svgsprites.less')
        .pipe(gulp.dest(config.src.less + 'static/svgsprites.less'));
});

gulp.task('svgmove', function () {
    return gulp.src(config.src.svg)
        .pipe(plumber())
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        .pipe(gulp.dest(config.dev.svg));
});

gulp.task('svg', ['svgsprites', 'ssmove', 'slmove']);

gulp.task('watch', ['browser-sync'], function () {
    gulp.watch(config.src.less + '**/*.less', ['less', browserSync.reload]);
    gulp.watch(config.src.pug + '**/*.pug', ['pug', browserSync.reload]);
    gulp.watch(config.src.js + '**/*.js', ['concat', browserSync.reload]);
    gulp.watch(config.src.images + '**', ['img', browserSync.reload]);
    gulp.watch(config.src.svg, ['svgmove', browserSync.reload]);
    //gulp.watch(['src/js/**/**/*.js', '!src/js/app.compile.js'], ['browserify', browserSync.reload]);
    //gulp.watch("src/*.html").on("change", browserSync.reload);
});

gulp.task('svg', ['svgsprites', 'svgmove']);

gulp.task('prod', ['less', 'pug', 'svg', 'sprites', 'imagemin', 'mincss', 'uglifyJs', 'html']);

